FROM python:3.12 as requirements
WORKDIR /workdir
RUN pip install poetry
COPY ./pyproject.toml ./poetry.lock* ./
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

FROM python:3.12
WORKDIR /workdir
COPY ./movies movies
COPY ./RSA256-public.pem .
COPY --from=requirements /workdir/requirements.txt requirements.txt
RUN pip install --no-cache-dir --upgrade -r requirements.txt

ENV DB_CONNECTION_STRING="sqlite:///db.sqlite"
ENV AUTH_SERVER_PUBLIC_KEY_PATH="./RSA256-public.pem"

# Handle requests with single process for more predictable cpu and memory behaviour
# Assuming deployment to an orchestrator which will handle process scaling and TLS termination
CMD ["uvicorn", "movies.main:app", "--host", "0.0.0.0", "--port", "80", "--no-server-header"] 
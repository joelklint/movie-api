""" 
Everything in this file, I would normally handle outside of the application memory boundary.
I am doing it this way now because this is not a production system.

How I would solve it would depend on situation. Examples are:
- Create/Update database with migrations during deployment or use a schemaless database
- Seed database during deployment or as a one-off job from another pod within live enviornment
"""

import logging
import math
from multiprocessing.dummy import Pool
from movies.adapters.settings import Settings
from movies.adapters.omdb_adapter import OmdbAdapter
from movies.persistence import database, models
from movies.application import schemas

log = logging.getLogger(__name__)

# We should not add a token to a code base for security reasons.
PRE_GENERATED_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJKb2VsIEtsaW50Iiwic3ViIjoiRlx1MDBmNnJzXHUwMGY2a3NrYW5pbiIsImF1ZCI6Im1vdmllLWFwaSIsIkNvbmdyYXR1bGF0aW9ucyEiOiJZb3UgZm91bmQgdGhlIGVhc3RlciBlZ2chIFx1ZDgzZVx1ZGQ1YVx1ZDgzY1x1ZGZjNiJ9.WxEWfnih-PiKE5HpluD5yvqDV7gLhMCflOpGeP5i9E39qXZYPXoAv2xXboW1FhItZhClnuvjlUZgS011DB4P3Tw4ClmTIdtqV8v2vgy1xO8yWYaelKByYD3hWf1bUl8_IA4HwNNhaIsmEt2XFxnBMupzF4YAzD0oG5OfwbGngagf2fZKX9sb_lVrUnRDSf5xhQ12H-KvOCuDp8LGN3wjm8oybGTAUxme3XgqJ_Y8FLajdqG3nCTaBMwXiVcsMUi34eBQXqK0ANJLFL2eCoSmb15SLEo3y0EITTgq75uOP6zL-phTIHyw5RJFfq0HS2JOCZzy-liOzHZsJ4mtyJ8dbA"

def flat_map(x_list):
    result = []
    for x in x_list:
        result.extend(x)
    return result

def prepare_database_in_a_non_production_way(search_query: str, settings: Settings) -> int:
    # Create database
    engine = database.get_db_engine(settings.db_connection_string)
    database.Base.metadata.create_all(bind=engine)


    # Seed database
    db = database.get_db_session(engine)
    if db.query(models.Movie).count() > 0:
        return 0

    seed_target = 100
    log.info("Initiating database seeding of %s movies", seed_target)

    omdb = OmdbAdapter(settings.omdb_api_key)

    # Use threads as this is I/O bound
    with Pool(min(seed_target, 100)) as pool:
        # Assume 10 movies per page (as of time of authoring)
        target_search_pages = range(math.ceil(seed_target / 10))
        search_pages = pool.map(lambda page: omdb.search_movies(search_query, page+1), target_search_pages)
        sparse_movies = flat_map(filter(lambda page: page is not None, search_pages))
        movie_ids = list(map(lambda x: x.imdbID, sparse_movies[:seed_target]))

        identified_movie_count = len(list(movie_ids))
        log_level = logging.INFO if identified_movie_count == seed_target else logging.WARN
        log.log(log_level, "Identified %s movies matching the search query %s", identified_movie_count, search_query)

        detailed_movies = pool.map(omdb.get_movie_by_id, movie_ids)

    db_movies = list(map(lambda x: models.Movie(**schemas.Movie.model_dump(x)), detailed_movies))
    db.add_all(db_movies)
    db.commit()
    db.close()

    seed_count = len(db_movies)
    log.info("Database populated with %s movies", seed_count)
    return seed_count

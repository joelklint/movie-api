"""
Entrypoint for solution
"""

import logging
import os
from movies.application.depedencies import get_settings
from movies.application.service import app # pylint: disable=W0611
from movies import the_non_production_stuff

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

the_non_production_stuff.prepare_database_in_a_non_production_way("Harry Potter", get_settings())

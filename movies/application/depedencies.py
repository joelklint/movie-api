"""
This file houses dependency injection definitions
"""

from functools import lru_cache
from typing import Annotated, Any, Generator
from fastapi import Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from sqlalchemy import Engine
from sqlalchemy.orm import Session
from movies.adapters.omdb_adapter import OmdbAdapter
from movies.adapters.settings import Settings
from movies.persistence import database


@lru_cache
def get_settings():
    return Settings() # type: ignore
InjectedSettings = Annotated[Settings, Depends(get_settings)]


def get_db_engine(settings: InjectedSettings) -> Generator[Engine, Any, Any]:
    yield database.get_db_engine(settings.db_connection_string)
InjectedDbEngine = Annotated[Engine, Depends(get_db_engine)]


def get_db(db_engine: InjectedDbEngine) -> Generator[Session, Any, Any]:
    db = database.get_db_session(db_engine)
    try:
        yield db
    finally:
        db.close()
InjectedDb = Annotated[Session, Depends(get_db)]


def get_omdb(settings: InjectedSettings) -> Generator[OmdbAdapter, Any, Any]:
    yield OmdbAdapter(settings.omdb_api_key)
InjectedOmdb = Annotated[OmdbAdapter, Depends(get_omdb)]


bearer_scheme = HTTPBearer(description="Token issued by authentication server")
InjectedToken = Annotated[HTTPAuthorizationCredentials, Depends(bearer_scheme)]

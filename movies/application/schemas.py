"""
This file houses the application data model
"""

from typing import List
from pydantic import BaseModel, ConfigDict, constr, model_validator

class Rating(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    Source: str | None
    Value: str | None

class Movie(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    imdbID : str
    imdbVotes : str | None
    imdbRating : str | None
    Title : str
    Year : str | None
    Rated : str | None
    Released : str | None
    Runtime : str | None
    Genre : str | None
    Director : str | None
    Writer : str | None
    Actors : str | None
    Plot : str | None
    Language : str | None
    Country : str | None
    Awards : str | None
    Poster : str | None
    Metascore : str | None
    Type : str | None
    DVD : str | None
    BoxOffice : str | None
    Production : str | None
    Website : str | None
    Ratings : List[Rating | None] | None

class CreateMovieRequest(BaseModel):
    Title: constr(min_length=1)

class CreateMovieResponse(BaseModel):
    imdbID: str

class ListMovieRequest(BaseModel):
    offset: int = 0
    limit: int = 10

class DeleteMovieRequest(BaseModel):
    imdbID: str

class GetMovieRequest(BaseModel):
    imdbID: str | None = None
    Title: str | None = None

    @model_validator(mode='after')
    def validate_exactly_one_value(self):
        if self.imdbID and self.Title or not self.imdbID and not self.Title:
            raise ValueError("You must specify exactly one parameter")
        return self

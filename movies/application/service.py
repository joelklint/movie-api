"""
This file houses application logic and exposes it over HTTP. 

I usually prefer to separate application logic from exposure channels such as
http, grpc, events, etc but if felt a bit overkill here
"""

import base64
import json
import logging
from jose import jwt
from fastapi import FastAPI, HTTPException
from movies.application.depedencies import InjectedDb, InjectedOmdb, InjectedSettings, InjectedToken
from movies.persistence import models
from movies.application import schemas
from movies.the_non_production_stuff import PRE_GENERATED_TOKEN

# Different loggers for different purposes, but identically configured here 🤓
log = logging.getLogger(__name__)
audit_log = logging.getLogger(__name__)


app = FastAPI(
    title="Movies API",
    description=f"""
Hello and welcome 👋

Below you will find a pre-generated token, which can be used for testing protected endpoints 🔒

Committing tokens to revision control is a security issue and is not recommended
    
    {PRE_GENERATED_TOKEN}
""",
)


@app.post('/movies.list')
def list_movies(request: schemas.ListMovieRequest, db: InjectedDb) -> list[schemas.Movie]:
    movies = db.query(models.Movie).order_by(models.Movie.Title).offset(request.offset).limit(request.limit).all()
    return list(map(schemas.Movie.model_validate, movies))


@app.post('/movies.get')
def get_movie(request: schemas.GetMovieRequest, db: InjectedDb) -> schemas.Movie:
    query = db.query(models.Movie)
    if request.imdbID:
        query = query.filter(models.Movie.imdbID == request.imdbID)
    if request.Title:
        query = query.filter(models.Movie.Title == request.Title)
    result = query.limit(2).all()
    if len(result) < 1:
        raise HTTPException(status_code=404)
    if len(result) > 1:
        raise HTTPException(
            status_code=400,
            detail="Your request matched more than one movie. Make a more detailed request"
        )

    return schemas.Movie.model_validate(result[0])


@app.post('/movies.create')
def create_movie(movie: schemas.CreateMovieRequest, db: InjectedDb, omdb: InjectedOmdb):
    omdb_movie = omdb.get_movie_by_title(movie.Title)
    already_exists = db.query(models.Movie).filter(models.Movie.imdbID == omdb_movie.imdbID).count() > 0
    if already_exists:
        raise HTTPException(status_code=400, detail="Movie already exists")
    db.add(models.Movie(**omdb_movie.model_dump()))
    db.commit()
    return schemas.CreateMovieResponse(imdbID=omdb_movie.imdbID)


@app.post('/movies.delete')
def delete_movie(request: schemas.DeleteMovieRequest, token: InjectedToken, db: InjectedDb, settings: InjectedSettings) -> None:
    # We could read public key once and store in-memory for speed improvements
    with open(settings.auth_server_public_key_path, 'r', encoding='utf-8') as public_key:
        decoded_token = jwt.decode(token=token.credentials, key=public_key.read(), audience="movie-api")
        # Here we can execute any authorisation logic we'd like
        audit_log.info("user %s deletes movie %s", decoded_token['sub'], request.imdbID)

    delete_count = db.query(models.Movie).filter(models.Movie.imdbID == request.imdbID).delete()
    if delete_count > 0:
        db.commit()
        log.info("Movie %s deleted", request.imdbID)
    else:
        raise HTTPException(status_code=404, detail="Movie does not exist")

@app.get('/super-epic-quest')
def super_epic_quest():
    msg = "eyJxdWVzdCI6ICJIdXJyYXkhIFRoZSBzdGFydCBvZiB0aGUgZXBpYyBxdWVzdCB5b3UgbWFuYWdlZCB0byBmaW5kLiBUaGUgZWFzdGVyIGVnZyBidXQgY2FuIHlvdSBmaW5kPyBZb3UgZm91bmQgaXQgd2hlbiB5b3Ugc2VlIHRoZSB0cm9waHkgeW91IGtub3cuIFllcywgaHJybW1tLiIsInF1ZXN0X2dpdmVyIjogImh0dHBzOi8vcGJzLnR3aW1nLmNvbS9wcm9maWxlX2ltYWdlcy83NTg4NzA5NDg5ODAyODU0NDAvc3FsSGYzSW1fNDAweDQwMC5qcGcifQ=="
    return json.loads(base64.b64decode(msg.encode('utf-8')).decode('utf-8'))

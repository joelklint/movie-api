"""
This file houses the persistence data model
"""

from sqlalchemy import Column, String, JSON
from .database import Base

class Movie(Base):
    __tablename__ = "movies"

    imdbID = Column(String, primary_key=True, index=True)
    imdbVotes = Column(String)
    imdbRating = Column(String)
    Title = Column(String)
    Year = Column(String)
    Rated = Column(String)
    Released = Column(String)
    Runtime = Column(String)
    Genre = Column(String)
    Director = Column(String)
    Writer = Column(String)
    Actors = Column(String)
    Plot = Column(String)
    Language = Column(String)
    Country = Column(String)
    Awards = Column(String)
    Poster = Column(String)
    Metascore = Column(String)
    Type = Column(String)
    DVD = Column(String)
    BoxOffice = Column(String)
    Production = Column(String)
    Website = Column(String)
    Ratings = Column(JSON)

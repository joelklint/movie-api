"""
This file houses infrastructure related to the database
"""

from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import sessionmaker, Session, declarative_base

def get_db_engine(connection_string: str) -> Engine:
    return create_engine(connection_string, connect_args={"check_same_thread": False})

def get_db_session(engine: Engine) -> Session:
    return sessionmaker(autocommit=False, autoflush=False, bind=engine)()

Base = declarative_base()

"""
This file houses the adapter to the API of The Open Movie Database: https://www.omdbapi.com
"""

import urllib.parse
import requests
from pydantic import BaseModel, ConfigDict
from movies.application import schemas

class SearchMovie(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    imdbID : str

class SearchResult(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    Search : list[SearchMovie] | None = None
    totalResults : int | None = None
    Response : bool

class OmdbAdapter:
    def __init__(self, api_key: str):
        self.timeout = 30 # Very long timeout but I'd rather have you wait than get an exception 🐌
        self.base_url = f"https://www.omdbapi.com/?apikey={urllib.parse.quote(api_key)}"

    def get_movie_by_title(self, name: str) -> schemas.Movie:
        encoded = urllib.parse.quote(name)
        response = requests.get(f"{self.base_url}&type=movie&t={encoded}", timeout=self.timeout)
        return schemas.Movie.model_validate_json(response.text)

    def get_movie_by_id(self, movie_id: str) -> schemas.Movie:
        encoded = urllib.parse.quote(movie_id)
        response = requests.get(f"{self.base_url}&type=movie&i={encoded}", timeout=self.timeout)
        return schemas.Movie.model_validate_json(response.text)

    def search_movies(self, name: str, page: int = 1) -> list[SearchMovie] | None:
        encoded = urllib.parse.quote(name)
        response = requests.get(f"{self.base_url}&type=movie&s={encoded}&page={page}", timeout=self.timeout)
        parsed = SearchResult.model_validate_json(response.text)
        return None if not parsed.Search else parsed.Search

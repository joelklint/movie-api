"""
This file houses the adapter to accessing runtime configuration
"""

from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    omdb_api_key: str
    db_connection_string: str
    auth_server_public_key_path: str

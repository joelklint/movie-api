"""
This file houses test fixtures
"""

from typing import Any, Generator
import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from movies.application.depedencies import get_settings
from movies.application.service import app
from movies.adapters.settings import Settings
from movies.persistence import database
from movies.the_non_production_stuff import PRE_GENERATED_TOKEN

def get_fake_settings():
    return Settings(
        db_connection_string="sqlite:///./test.sqlite",
        omdb_api_key="fake-key",
        auth_server_public_key_path="./RSA256-public.pem"
    )

@pytest.fixture
def token() -> Generator[str, Any, Any]:
    yield PRE_GENERATED_TOKEN

@pytest.fixture
def settings() -> Generator[Settings, Any, Any]:
    yield get_fake_settings()

@pytest.fixture
def test_client() -> Generator[TestClient, Any, Any]:
    client = TestClient(app)
    app.dependency_overrides[get_settings] = get_fake_settings
    yield client

@pytest.fixture
def db(settings) -> Generator[Session, Any, Any]:
    engine = database.get_db_engine(settings.db_connection_string)
    database.Base.metadata.create_all(bind=engine) # Create database and all tables
    db = database.get_db_session(engine)
    yield db
    db.close()
    database.Base.metadata.drop_all(bind=engine) # Drop all tables

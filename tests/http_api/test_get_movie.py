import json
from tests.data_generator import generate_movie_json
from movies.persistence import models


def test_should_not_accept_zero_parameters(test_client):
    response = test_client.post('/movies.get', json={})

    assert response.status_code == 422
    assert "You must specify exactly one parameter" in response.json()['detail'][0]['msg']

def test_should_not_accept_both_parameters(test_client):
    response = test_client.post('/movies.get', json={
        "id": "some-id",
        "title": "some-title"
    })

    assert response.status_code == 422
    assert "You must specify exactly one parameter" in response.json()['detail'][0]['msg']

def test_should_be_able_to_get_movie_by_id(test_client, db):
    movie_a = json.loads(generate_movie_json(id = "a", title="bb"))
    movie_b = json.loads(generate_movie_json(id = "b", title="aa"))
    db.add_all([
        models.Movie(**movie_a), 
        models.Movie(**movie_b), 
    ])
    db.commit()

    response = test_client.post('/movies.get', json={
        "imdbID": "a"
    })

    assert response.status_code == 200
    assert response.json() == movie_a

def test_should_be_able_to_get_movie_by_title(test_client, db):
    movie_a = json.loads(generate_movie_json(id = "bb", title="a"))
    movie_b = json.loads(generate_movie_json(id = "aa", title="b"))
    db.add_all([
        models.Movie(**movie_a), 
        models.Movie(**movie_b), 
    ])
    db.commit()

    response = test_client.post('/movies.get', json={
        "Title": "b"
    })

    assert response.status_code == 200
    assert response.json() == movie_b

def test_should_return_HTTP_404_if_movie_does_not_exit(test_client, db):
    response = test_client.post('/movies.get', json={
        "imdbID": "a"
    })

    assert response.status_code == 404

def test_should_return_HTTP_400_if_more_than_one_movie_found(test_client, db):
    db.add_all([
        models.Movie(**json.loads(generate_movie_json(id = "a", title="duplicate-title"))), 
        models.Movie(**json.loads(generate_movie_json(id = "b", title="duplicate-title"))), 
    ])
    db.commit()
    response = test_client.post('/movies.get', json={
        "Title": "duplicate-title"
    })

    assert response.status_code == 400
    assert "Your request matched more than one movie" in response.json()['detail']
import json
from tests.data_generator import generate_movie_json
from movies.persistence import models

def test_should_require_token(test_client):
    response = test_client.post(
        '/movies.delete', 
        headers={}, 
        json={}
    )

    assert response.status_code == 403


def test_should_require_id(test_client, token):
    response = test_client.post(
        '/movies.delete', 
        headers={"Authorization": f"Bearer {token}"}, 
        json={}
    )

    assert response.status_code == 422
    assert "Field required" in response.json()['detail'][0]['msg']
    assert response.json()['detail'][0]['loc'] == ["body","imdbID"]

def test_should_delete_data_if_it_exists(test_client, token, db):
    db.add(models.Movie(**json.loads(generate_movie_json(id="some-id"))))
    db.commit()

    response = test_client.post(
        '/movies.delete', 
        headers={"Authorization": f"Bearer {token}"}, 
        json={"imdbID": "some-id"}
    )

    assert response.status_code == 200
    assert db.query(models.Movie).filter(models.Movie.imdbID == "some-id").count() == 0

def test_should_return_HTTP_400_if_data_does_not_exist(test_client, token, db):
    response = test_client.post(
        '/movies.delete', 
        headers={"Authorization": f"Bearer {token}"}, 
        json={"imdbID": "some-id"}
    )

    assert response.status_code == 404


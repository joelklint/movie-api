import json
import pytest
from movies.persistence import models
from tests.data_generator import generate_movie_json

def test_list_movies_returns_entire_objects(test_client, db):
    movie_json = json.loads(generate_movie_json())
    db.add(models.Movie(**movie_json))
    db.commit()

    response = test_client.post('/movies.list', json={})

    assert response.status_code == 200
    assert response.json() == [movie_json]


def test_list_movies_is_sorted_by_title(test_client, db):
    a = json.loads(generate_movie_json(id = "e", title="a"))
    b = json.loads(generate_movie_json(id = "f", title="b"))
    c = json.loads(generate_movie_json(id = "c", title="c"))
    d = json.loads(generate_movie_json(id = "d", title="d"))
    e = json.loads(generate_movie_json(id = "a", title="e"))
    f = json.loads(generate_movie_json(id = "b", title="f"))
    db.add_all([
        models.Movie(**b), 
        models.Movie(**a), 
        models.Movie(**d), 
        models.Movie(**c), 
        models.Movie(**f), 
        models.Movie(**e)
    ])
    db.commit()

    response = test_client.post('/movies.list', json={})

    assert response.status_code == 200
    assert response.json() == [a, b, c, d, e, f]

@pytest.mark.parametrize("limit", [1, 3, 5, 6, 9])
def test_list_movies_returns_no_more_entities_than_specified(test_client, db, limit: int):
    for x in range(10):
        db.add(models.Movie(**json.loads(generate_movie_json(id = f'{x}'))))
    db.commit()

    response = test_client.post('/movies.list', json={
        "limit": limit
    })

    assert response.status_code == 200
    assert len(response.json()) == limit

def test_list_movies_returns_10_entities_as_default(test_client, db):
    for x in range(15):
        db.add(models.Movie(**json.loads(generate_movie_json(id = f'{x}'))))
    db.commit()

    response = test_client.post('/movies.list', json={})

    assert response.status_code == 200
    assert len(response.json()) == 10

@pytest.mark.parametrize("offset", [1, 3, 5, 6, 9])
def test_list_movies_offsets_as_specified(test_client, db, offset: int):
    for x in range(10):
        db.add(models.Movie(**json.loads(generate_movie_json(id = f'{x}', title = f'{x}'))))
    db.commit()

    response = test_client.post('/movies.list', json={
        "offset": offset
    })

    assert response.status_code == 200
    assert response.json()[0]['Title'] == f'{offset}'

def test_list_movies_offsets_0_as_default(test_client, db):
    for x in range(15):
        db.add(models.Movie(**json.loads(generate_movie_json(id = f'{x}', title = f'{x}'))))
    db.commit()

    response = test_client.post('/movies.list', json={})

    assert response.status_code == 200
    assert response.json()[0]['Title'] == '0'
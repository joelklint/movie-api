import json
import responses
from tests.data_generator import generate_movie_json
from movies.persistence import models


def test_should_require_title(test_client):
    response = test_client.post('/movies.create', json={})

    assert response.status_code == 422
    assert "Field required" in response.json()['detail'][0]['msg']
    assert response.json()['detail'][0]['loc'] == ["body","Title"]

@responses.activate
def test_should_create_movie_with_data_from_omdb(test_client, settings, db):
    responses.add(
        method = responses.GET,
        url=f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&t=Harry%20Potter",
        json=json.loads(generate_movie_json(id="omdb-id", title="omdb-title"))
    )
    response = test_client.post('/movies.create', json={
        "Title": "Harry Potter"
    })

    assert response.status_code == 200
    assert db.query(models.Movie).filter(models.Movie.imdbID == "omdb-id", models.Movie.Title == "omdb-title").count() == 1 # This assertion could be more exhaustive

@responses.activate
def test_should_return_id_in_response(test_client, settings, db):
    responses.add(
        method = responses.GET,
        url=f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&t=Harry%20Potter",
        json=json.loads(generate_movie_json(id="omdb-id", title="omdb-title"))
    )
    response = test_client.post('/movies.create', json={
        "Title": "Harry Potter"
    })

    assert response.status_code == 200
    assert response.json() == {"imdbID": "omdb-id"}

@responses.activate
def test_should_return_HTTP_400_if_movie_already_exists(test_client, settings, db):
    movie = json.loads(generate_movie_json(id="some-id"))
    db.add(models.Movie(**movie))
    db.commit()
    responses.add(
        method = responses.GET,
        url=f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&t=Harry%20Potter",
        json=movie
    )

    response = test_client.post('/movies.create', json={
        "Title": "Harry Potter"
    })

    assert response.status_code == 400
    assert response.json()['detail'] == "Movie already exists"

@responses.activate
def test_should_return_HTTP_422_if_empty_title_provided(test_client, settings):
    responses.add(
        method=responses.GET,
        url=f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&t=",
        json={"Response": "False", "Error": "Incorrect IMDb ID."}
    )

    response = test_client.post('/movies.create', json={
        "Title": ""
    })

    assert response.status_code == 422
    assert response.json()['detail'][0]['loc'] == ["body","Title"]
    assert response.json()['detail'][0]['msg'] == "String should have at least 1 characters"


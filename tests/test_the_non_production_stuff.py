import json
import responses
from movies import the_non_production_stuff
from movies.persistence import models
from tests.data_generator import generate_movie_json

@responses.activate
def test_should_load_100_movies_if_database_empty(settings, db):
    search_query = "Marvel"
    movies = list(map(lambda id: json.loads(generate_movie_json(id = f"{id}")), range(150))) # Provide movie excess to test we don't overfill
    for page in range(15):
        responses.get(
            f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&s={search_query}&page={page+1}",
            status = 200,
            json={
                "totalResults": -1,
                "Response": True,
                "Search": movies[page*10:(page+1)*10]
            }
        )

    for i, movie in enumerate(movies):
        responses.get(
            f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&i={i}",
            status = 200,
            json=movie
        )

    created_count = the_non_production_stuff.prepare_database_in_a_non_production_way(search_query, settings)

    assert created_count == 100
    assert db.query(models.Movie).count() == 100

@responses.activate
def test_should_load_movies_if_there_are_less_than_100_matches(settings, db):
    search_query = "Marvel"
    movies = list(map(lambda id: json.loads(generate_movie_json(id = f"{id}")), range(50))) # Provide lack of movie to test we don't fail
    for page in range(5):
        responses.get(
            f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&s={search_query}&page={page+1}",
            status = 200,
            json={
                "totalResults": -1,
                "Response": True,
                "Search": movies[page*10:(page+1)*10]
            }
        )
    for page in range(5,10):
        responses.get(
            f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&s={search_query}&page={page+1}",
            status = 200,
            json={
                "Response": False,
                "Error": "Movie not found!"
            }
        )

    for i, movie in enumerate(movies):
        responses.get(
            f"https://www.omdbapi.com/?apikey={settings.omdb_api_key}&type=movie&i={i}",
            status = 200,
            json=movie
        )

    created_count = the_non_production_stuff.prepare_database_in_a_non_production_way(search_query, settings)

    assert created_count == 50
    assert db.query(models.Movie).count() == 50

def test_should_not_load_movies_if_db_not_empty(settings, db):
    db.add(models.Movie(**json.loads(generate_movie_json())))
    db.commit()

    created_count = the_non_production_stuff.prepare_database_in_a_non_production_way("anything", settings)

    assert created_count == 0
    assert db.query(models.Movie).count() == 1

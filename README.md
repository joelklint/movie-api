# Movie api

A movie API powered by [OMDb API](https://www.omdbapi.com) 🎥🍿


A good starting point for getting to know the solution would be [movies/main.py](movies/main.py) and [movies/application/service.py](movies/application/service.py). The solution consists of:
- Persistence layer powered by [SQLAlchemy](https://www.sqlalchemy.org)
- Application layer powered by [FastAPI](https://fastapi.tiangolo.com)
- Adapters for other integrations


## Comments from author
- I have assumed that this API is deployed to a container-based orchestrator hosted at GCP, which handles scaling and TLS termination.
- I have assumed that this API is part of a larger solution which has a central authentication service using asymmetric signatures.
- I have committed [RSA256-private.pem](RSA256-private.pem) in this repository which represents the private key of the authentication service. Its purpose is to enable generation of additional test tokens.
- I am defaulting to an SQLite database which is not production grade. I did that to simplify local testing

## Interacting with the API quickly
1. Follow Docker instructions below
2. Visit http://localhost:8080/docs


## Run project
### Docker
```sh
docker build -t movie-api .

docker run --rm -p 8080:80 --env OMDB_API_KEY=[TOKEN_HERE] movie-api
```
#### Dependencies
- [Docker](https://www.docker.com)

## Develop
```sh
poetry shell

poetry install

OMDB_API_KEY=[TOKEN_HERE] DB_CONNECTION_STRING=sqlite:///./db.sqlite AUTH_SERVER_PUBLIC_KEY_PATH=./RSA256-public.pem uvicorn movies.main:app --reload
```
#### Dependencies
- [Poetry](https://python-poetry.org)

## Test
```sh
poetry shell

poetry install

# Once
python -m pytest

# Watch mode
ptw

# Get test coverage - macOS
coverage run --branch -m pytest && coverage html && open htmlcov/index.html
```
#### Dependencies
- [Poetry](https://python-poetry.org)

## Lint
```sh
poetry shell

poetry install

pylint movies tests --disable C0116,C0115
```
